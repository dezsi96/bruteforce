﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PicBook.Repository.EntityFramework
{
    public abstract class GennericCRUDRepository<TEntity> where TEntity:class
    {
        protected PicBookDBContext context;

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            return await context.Set<TEntity>().ToListAsync<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> FindBy(Expression<Func<TEntity, bool>> filterExpression)
        {         
            var entites=context.Set<TEntity>().Where(filterExpression);

            return await entites.ToListAsync();
        }

        public virtual async Task Create(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            await context.SaveChangesAsync();
        }

        public virtual async Task Update(TEntity entity)
        {
            context.Entry<TEntity>(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        public virtual async Task Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            await context.SaveChangesAsync();
        }
    }
}
