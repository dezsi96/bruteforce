﻿using Microsoft.EntityFrameworkCore;
using PicBook.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicBook.Repository.EntityFramework
{
    public class UserRepository : GennericCRUDRepository<User>
    {
        public UserRepository(PicBookDBContext context)
        {
            this.context = context;
        }

        public async Task<User> GetUserAsync(string nameIdentifier)
        {
            var users= await FindBy(u => u.NameIdentifier == nameIdentifier);
            User user = users.First();
            var photos = context.Photos.Where(photo => photo.User == user).ToListAsync();
            user.Photos = photos.Result;
            var albums = context.Album.Where(album => album.User == user).ToListAsync();
            user.Albums = albums.Result;
            return user;
        }
    }
}
