﻿using Microsoft.EntityFrameworkCore;
using PicBook.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace PicBook.Repository.EntityFramework
{
    public class PicBookDBContext:DbContext
    {
        public PicBookDBContext(DbContextOptions<PicBookDBContext> options):base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Album> Album { get; set; }
    }
}
