﻿using Microsoft.AspNetCore.Http;

namespace PicBook.Web.Models.Photos
{
    public class FileInputModel
    {
        public IFormFile FileToUpload { get; set; }
    }
}
