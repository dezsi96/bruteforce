﻿using PicBook.Domain;
using System.Collections.Generic;

namespace PicBook.Web.Models.Photos
{
    public class FilesViewModel
    {
        public List<FileDetails> Files { get; set; }
        public List<Album> Albums { get; set; }

        public FilesViewModel()
        {
            Files = new List<FileDetails>();
            Albums = new List<Album>();
        }
    }
}
