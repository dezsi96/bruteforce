﻿namespace PicBook.Web.Models.Photos
{
    public class FileDetails
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Tags { get; set; }
    }
}
