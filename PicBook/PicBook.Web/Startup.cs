﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using PicBook.ApplicationService;
using PicBook.Repository.EntityFramework;
using System.IO;

namespace PicBook.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder();
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }
            Configuration = builder.Build();
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/uploads")));

            services.Configure<MvcOptions>(
                options => options.Filters.Add(new RequireHttpsAttribute())
            );

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(o =>
                            {
                                o.LoginPath = new PathString("/Account/Login");
                                o.LogoutPath = new PathString("/Home/Index");
                            })
                    .AddFacebook(o =>
                            {
                                o.AppId = Configuration["AppId"];
                                o.AppSecret = Configuration["AppSecret"];
                                o.Scope.Add("email");
                                o.Fields.Add("name");
                                o.Fields.Add("email");
                                o.SaveTokens = true;
                            });

            services.AddDbContext<PicBookDBContext>(builder => builder.UseSqlServer(Configuration.GetConnectionString("SQLConnectionString")));

            services.AddScoped<UserRepository>();
            services.AddScoped<UserService>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var options = new RewriteOptions().AddRedirectToHttps(301, 44301);
            app.UseRewriter(options);
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }


            app.UseStaticFiles();
            app.UseMvc();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
