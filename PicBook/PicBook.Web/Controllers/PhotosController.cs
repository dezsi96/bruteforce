﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using PicBook.Web.Models.Photos;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using PicBook.ApplicationService;
using System;

namespace PicBook.Web.Controllers
{
    [Authorize]
    public class PhotosController : Controller
    {
        private readonly IFileProvider fileProvider;
        private readonly UserService userService;

        public PhotosController(IFileProvider fileProvider, UserService userService)
        {
            this.fileProvider = fileProvider;
            this.userService = userService;
        }

        public IActionResult Albums(string condition)
        {
            return GetUserAlbums();
        }
        public IActionResult Index(string condition = null, string albumname = null)
        {
            return Files(null, condition, albumname);
        }
        public IActionResult ShowPicture(string filename)
        {
            return Files(filename);
        }
        public IActionResult Upload(string filename)
        {
            return GetUserAlbums();
        }
        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            return RedirectToAction("Upload");

        }
        [HttpPost]
        public async Task<IActionResult> ShowPicture()
        {
            return RedirectToAction("ShowPicture");

        }
        [HttpPost]
        public async Task<IActionResult> Albums()
        {
            return RedirectToAction("Albums");

        }
        [HttpPost]
        public async Task<IActionResult> SearchPhoto(string condition)
        {
            return Files(null, condition);

        }
        //public IActionResult SearchPhoto(string condition)
        //{
        //    return Files(null, condition);
        //}
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file, string title, string tags)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");

            string fileEnd = file.GetFilename().Split('.').Last();
            string fileName = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            fileName += '.' + fileEnd;

            string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                    .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;

            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/uploads/"+identifier,
                fileName);
            var path2 = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/uploads/" + identifier);

            if (!Directory.Exists(path2))
            {
                Directory.CreateDirectory(path2);
            }
           

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
                //string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                //                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
                await userService.UploadImage(identifier, fileName,title, tags);
            }
            return RedirectToAction("Index");

        }
        [HttpPost]
        public async Task<IActionResult> UpdateImageDetails(string file, string title, string tags, string album)
        {
            title = StringValidate(title);
            tags = StringValidate(tags);
            if (file == null || file.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/uploads",
                file);
                string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
                await userService.UpdateImage(identifier, file, title, tags, album);
            return RedirectToAction("Index");

        }
        [HttpPost]
        public async Task<IActionResult> AddAlbum(string albumname)
        {
            albumname = StringValidate(albumname);
            if (albumname == null || albumname == String.Empty)
            {
                return Content("Empty albumname!");
            }
            string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
            await userService.AddAlbum(identifier, albumname);
            return RedirectToAction("Albums");
        }
        [HttpPost]
        public IActionResult GetUserAlbums()
        {
            var model = new FilesViewModel();
            var userAlbums = userService.GetAlbums(User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value).Result;
            foreach (var item in userAlbums)
            {
                model.Albums.Add(item);
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> UploadFiles(List<IFormFile> file, string title, string tags, string album)
        {
            title = StringValidate(title);
            tags = StringValidate(tags);
            if (file == null || file.Count == 0)
                return Content("files not selected");
            bool nagyobb = false;
            foreach (var item in file)
            {
                if (item.Length <= 5242880)
                {

                string fileName = DateTime.Now.ToString("yyyyMMdd_HHmmssffff");
                fileName +=item.GetFilename();

                string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;

                var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot/uploads/" + identifier,
                    fileName);
                var path2 = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot/uploads/" + identifier);

                if (!Directory.Exists(path2))
                {
                    Directory.CreateDirectory(path2);
                }


                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await item.CopyToAsync(stream);
                    //string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                    //                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
                    await userService.UploadImage(identifier, fileName, title, tags, album);
                    }
                }
                else
                {
                    nagyobb = true;
                }
            }
            if (!nagyobb)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return Content("There were bigger file(s) than 5Mb. That images haven't uploaded, the rest have.");
            }
        }
        public async Task<IActionResult> UploadFilePage()
        {
            return View("Upload");
        }
        [HttpPost]
        public async Task<IActionResult> UploadFileViaModel(FileInputModel model)
        {
            if (model == null ||
                model.FileToUpload == null || model.FileToUpload.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/uploads",
                model.FileToUpload.GetFilename());

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await model.FileToUpload.CopyToAsync(stream);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Files(string filename = null, string condition = null, string albumname = null)
        {
            var model = new FilesViewModel();
            var usersPhotos = userService.GetUsersPhotoNames(User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value).Result;
            string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                    .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
            var userAlbums = userService.GetAlbums(User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value).Result;
            foreach (var item in userAlbums)
            {
                model.Albums.Add(item);
            }
            foreach (var item in this.fileProvider.GetDirectoryContents(identifier))
            {
                foreach (var photo in usersPhotos)
                {
                    if (photo.FileName == item.Name && (filename == null || item.Name == filename))
                    {
                        if (albumname != null && albumname != String.Empty) 
                        {
                            if (photo.Album != null && photo.Album.Name == albumname)
                            {
                                model.Files.Add(
                                    new FileDetails { Name = item.Name, Path = item.PhysicalPath, Title = photo.Title, Tags = photo.Tags });
                            }
                        }
                        else
                        {
                            if (condition != null)
                            {
                                if ((photo.Title != null && photo.Title.ToUpper().Contains(condition.ToUpper())) || (photo.Tags != null && photo.Tags.ToUpper().Contains(condition.ToUpper())))
                                {
                                    model.Files.Add(
                                        new FileDetails { Name = item.Name, Path = item.PhysicalPath, Title = photo.Title, Tags = photo.Tags });
                                }
                            }
                            else
                            {
                                model.Files.Add(
                                    new FileDetails { Name = item.Name, Path = item.PhysicalPath, Title = photo.Title, Tags = photo.Tags });
                            }
                        }
                    }
                }
                //if(usersPhotos.Contains(item) && (filename == null || item.Name == filename))
                  //  model.Files.Add(
                    //    new FileDetails { Name = item.Name, Path = item.PhysicalPath});
            }
            return View(model);
        }

        public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var path = Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot/uploads", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }
        public async Task<IActionResult> ShowImage(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                    .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
            var path = Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot/uploads/"+identifier, filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
        
        private string StringValidate(string validate)
        {
            if (validate != null)
            {
                string OK = "QWERTZUIOPŐÚÖÜÓASDFGHJKLÉÁŰÍYXCVBNM,: ";
                string help = validate.ToUpper();
                string ret = "";
                for (int i = 0; i < help.Length; i++)
                {
                    if (OK.Contains(help[i]))
                    {
                        ret += validate[i];
                    }
                }
                return ret;
            }
            return null;
        }
    }
}