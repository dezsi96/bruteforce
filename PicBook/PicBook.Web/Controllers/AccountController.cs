﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using PicBook.Repository.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using PicBook.ApplicationService;

namespace PicBook.Web.Controllers
{
    public class AccountController : Controller
    {
        readonly UserService userService;

        public AccountController(UserService userService)
        {
            this.userService = userService;
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult LoginFacebook()
        {
            var authProps = new AuthenticationProperties() { RedirectUri = Url.Action("AuthCallback", "Account") };
            return Challenge(authProps, "Facebook");
        }

        public async Task<IActionResult> AuthCallback()
        {
            var facebookIdentity = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook" && i.IsAuthenticated);

            if (facebookIdentity == null)
            {
                return Redirect(Url.Action("Login", "Account"));
            }

            await userService.EnsureUserAsync(facebookIdentity.Claims);           

            return Redirect(Url.Action("Index", "Home"));
        }

        public async Task<IActionResult> Profile()
        {
            if(!User.Identity.IsAuthenticated)
                return Redirect(Url.Action("Index", "Home"));

            string identifier = User.Identities.FirstOrDefault(i => i.AuthenticationType == "Facebook")
                                        .Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;

            ViewData["Name"] = await userService.GetUsersName(identifier);
            ViewData["Email"] = await userService.GetUsersEmail(identifier);
            ViewData["Id"] = await userService.GetUsersID(identifier);
            return View();
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return Redirect(Url.Action("Index", "Home"));
        }
    }
}