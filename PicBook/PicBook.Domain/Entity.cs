﻿using System;

namespace PicBook.Domain
{
    public abstract class Entity
    {

        public Entity()
        {
            ID = Guid.NewGuid();
        }
        public Guid ID { get; internal set; }
    }
}
