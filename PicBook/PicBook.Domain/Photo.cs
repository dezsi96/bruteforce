using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PicBook.Domain
{
    public class Photo : Entity
    {
        
        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(255)]
        public string Tags { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }
        
        public Album Album { get; set; }

        [Required]
        public User User { get; set; }
        
    }
}