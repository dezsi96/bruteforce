﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PicBook.Domain
{
    public class Album : Entity
    {
        [StringLength(255)]
        public string NameIdentifier { get; set; }
        public ICollection<Photo> Photos { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public User User { get; set; }

        public Album()
        {
            Photos = new Collection<Photo>();
        }
        
    }
}
