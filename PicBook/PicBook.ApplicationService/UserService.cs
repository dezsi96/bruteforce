﻿using PicBook.Domain;
using PicBook.Repository.EntityFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PicBook.ApplicationService
{
    public class UserService
    {
        UserRepository userRepository;

        public UserService(PicBookDBContext context)
        {
            this.userRepository = new UserRepository(context);
        }
        public async Task EnsureUserAsync(IEnumerable<Claim> claims)
        {
            string nameIdentifier=claims.First(y=>y.Type==ClaimTypes.NameIdentifier).Value;

            var results=await userRepository.FindBy(x => x.NameIdentifier == nameIdentifier);
            var user = results.FirstOrDefault();

            if (user==null)
            {
                var email = claims.FirstOrDefault(x => x.Type == ClaimTypes.Email);
                var newUser = new User()
                {
                    NameIdentifier = nameIdentifier,
                    Email = email!=null?email.Value:null,
                    Name = claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value,
                };

                await userRepository.Create(newUser);
            }
        }
        public async Task<IEnumerable<Album>> GetAlbums(string nameIdentifier)
        {
            var user = await userRepository.GetUserAsync(nameIdentifier);
            return user.Albums;
        }
        public async Task<IEnumerable<Photo>> GetUsersPhotoNames(string nameIdentifier)
        {
            var user= await userRepository.GetUserAsync(nameIdentifier);
            return user.Photos;
        }
        public async Task UploadImage(string nameIdentifier,string fileName, string title = null, string tags = null, string albumname = null)
        {
            IEnumerable<User> users = await userRepository.FindBy(x => x.NameIdentifier == nameIdentifier);
            if (users.Count()!=0)
            {
                User user = users.First();
                var albums = await GetAlbums(user.NameIdentifier);
                var album = albums.SingleOrDefault(x => x.Name == albumname);
                Photo photo = new Photo() { FileName = fileName, User = user, Title = title, Tags = tags, Album = album };
                user.Photos.Add(photo);
                await userRepository.Update(user);
            }
            else
            {
                throw new Exception("User can't be found!");
            }
        }
        public async Task AddAlbum(string nameIdentifier, string AlbumName)
        {
            IEnumerable<User> users = await userRepository.FindBy(x => x.NameIdentifier == nameIdentifier);
            if (users.Count() != 0)
            {
                User user = users.First();
                user.Albums.Add(new Album() { Name = AlbumName});
                await userRepository.Update(user);
            }
            else
            {
                throw new Exception("User can't be found!");
            }
        }
        public async Task UpdateImage(string nameIdentifier, string fileName, string title = null, string tags = null, string album = null)
        {
            IEnumerable<User> users = await userRepository.FindBy(x => x.NameIdentifier == nameIdentifier);

            if (users.Count() != 0)
            {
                User user = users.First();
                user.Photos = await GetUsersPhotoNames(user.NameIdentifier) as ICollection<Photo>;
                Photo photo = user.Photos.SingleOrDefault(x => x.FileName == fileName);
                photo.Title = title;
                photo.Tags = tags;
                var albums = await GetAlbums(user.NameIdentifier);
                var specalbum = albums.SingleOrDefault(x => x.Name == album);
                photo.Album = specalbum;
                //user.Photos.Add(photo);
                await userRepository.Update(user);
            }
            else
            {
                throw new Exception("User can't be found!");
            }
        }
        public async Task<string> GetUsersName(string nameIdentifier)
        {
            var user = await userRepository.GetUserAsync(nameIdentifier);
            return user.Name;
        }
        public async Task<string> GetUsersEmail(string nameIdentifier)
        {
            var user = await userRepository.GetUserAsync(nameIdentifier);
            return user.Email;
        }
        public async Task<string> GetUsersID(string nameIdentifier)
        {
            var user = await userRepository.GetUserAsync(nameIdentifier);
            return user.ID.ToString();
        }

    }
}
